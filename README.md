Allows us to use StickyCode @Configuration tag to force fail-fast Spring configuration in our artifacts.
We don't wait until the methods get called to find our Grails config is missing - on load of the service,
 it detects it automatically.

